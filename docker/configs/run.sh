#!/bin/bash

export REPLICATION="${REPLICATION:-ARCHIVING}"
export PGDATA="${PGDATA:-/var/lib/pgsql/12/data}"
export PGUSER="${PGUSER:-postgres}"
export PATH=/usr/pgsql-12/bin:$PATH
export ARCHIVEDIR="${ARCHIVEDIR:-$PGDATA/../archivedir}"

export MASTER_IP="${MASTER_IP:-172.22.0.10}"
export STANDBY1_IP="${STANDBY1_IP:-172.22.0.11}"
export STANDBY2_IP="${STANDBY2_IP:-172.22.0.12}"

echo "=========================="
echo "PGDATA: $PGDATA"
echo "PGUSER: $PGUSER"
echo "=========================="

