#!/bin/bash

export REPLICATION="${REPLICATION:-ARCHIVING}"
export PGDATA="${PGDATA:-/var/lib/pgsql/12/data}"
export PGUSER="${PGUSER:-postgres}"
export PATH=/usr/pgsql-12/bin:$PATH
export ARCHIVEDIR="${ARCHIVEDIR:-$PGDATA/../archivedir}"

export MASTER_IP="${MASTER_IP:-172.22.0.10}"
export STANDBY1_IP="${STANDBY1_IP:-172.22.0.11}"
#export STANDBY2_IP="${STANDBY2_IP:-172.22.0.12}"

echo "=========================="
echo "REPLICATION: $REPLICATION"
echo "ROLE: $ROLE"
echo "PGUSER: $PGUSER"
echo "=========================="

#  USER, HOST
setup_ssh_connection()
{
    USER=$1
    HOST=$2
    ssh-keygen -t rsa -N '' -q -f ~/.ssh/id_rsa
    sshpass -p "$USER" ssh-copy-id -f $USER@$HOST

    status=$(ssh $USER@$HOST echo ok 2>&1)
    if [[ $status == ok ]] ; then
        echo "SSH connection ok"
    else
        echo "password-less ssh connection setup failed on $ROLE."
        exit
    fi
}


setup_master()
{
    # initialize cluster
    initdb

    # copy appropriate configurations.
    cat /configs/pg_hba.conf >> $PGDATA/pg_hba.conf
    cp /configs/postgresql.auto.conf.master $PGDATA/postgresql.auto.conf

    # start server
    pg_ctl -w start
}

setup_standby()
{
    until psql -h 172.22.0.10 -U $PGUSER -c '\q'; do
        >&2 echo "Postgres is unavailable - sleeping"
        sleep 1
    done

    # take backup
    pg_basebackup -h 172.22.0.10 -p 5432 -U $PGUSER -D $PGDATA -Fp -Xs -P

    # start server
    pg_ctl -w start

    psql -h 172.22.0.10 -p 5432 -U $PGUSER -x -c "create table testtab(a int primary key, b text);"
    psql -h 172.22.0.10 -p 5432 -U $PGUSER -x -c "insert into testtab  select a , md5(random()::text) from generate_series(1,100000) a;"
    psql -h 172.22.0.10 -p 5432 -U $PGUSER -x -c "create publication foopub for table testtab;"

    psql -x -c "create table testtab(a int primary key, b text);"
    psql -x -c "create subscription foosub connection 'host=172.22.0.10 port=5432 dbname=postgres user=postgres' publication foopub;"
    sleep 2
    psql -x -c "select count(*) from testtab;"


    psql -x -c "create table testtab_2(a int primary key, b text);"
    psql -x -c "insert into testtab_2  select 1, md5(random()::text) from generate_series(1,100000);"
    psql -x -c "create publication foopub_2 for table testtab_2;"

    psql  -h 172.22.0.10 -p 5432 -U $PGUSER -x -c "create table testtab_2(a int, b text);"
    psql  -h 172.22.0.10 -p 5432 -U $PGUSER -x -c "create subscription foosub_2 connection 'host=172.22.0.11 port=5432 dbname=postgres user=postgres' publication foopub_2;"
    sleep 2
    psql  -h 172.22.0.10 -p 5432 -U $PGUSER -x -c "select count(*) from testtab_2;"

}

# create archive directory
mkdir -p $ARCHIVEDIR
mkdir -p ~/.ssh && chmod 0700 ~/.ssh

case $ROLE in
    master)
        setup_master
        ;;
    standby)
        setup_standby standby
        ;;
    standby2)
        setup_standby standby2
        ;;
    *)
        echo "invalid option: $@"
        ;;
esac
