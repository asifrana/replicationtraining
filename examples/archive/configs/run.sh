#!/bin/bash

export REPLICATION="${REPLICATION:-ARCHIVING}"
export PGDATA="${PGDATA:-/var/lib/pgsql/12/data}"
export PGUSER="${PGUSER:-postgres}"
export PATH=/usr/pgsql-12/bin:$PATH
export ARCHIVEDIR="${ARCHIVEDIR:-$PGDATA/../archivedir}"

export MASTER_IP="${MASTER_IP:-172.22.0.10}"
export STANDBY1_IP="${STANDBY1_IP:-172.22.0.11}"
#export STANDBY2_IP="${STANDBY2_IP:-172.22.0.12}"

echo "=========================="
echo "REPLICATION: $REPLICATION"
echo "ROLE: $ROLE"
echo "PGUSER: $PGUSER"
echo "=========================="

#  USER, HOST
setup_ssh_connection()
{
    USER=$1
    HOST=$2
    ssh-keygen -t rsa -N '' -q -f ~/.ssh/id_rsa
    sshpass -p "$USER" ssh-copy-id -f $USER@$HOST

    status=$(ssh $USER@$HOST echo ok 2>&1)
    echo "Status: $status"
    if [[ $status == ok ]] ; then
        echo "Connection Ok"
    else
        echo "password-less ssh connection setup failed on $ROLE."
        exit
    fi
}


setup_master()
{
    # initialize cluster
    initdb

    # copy appropriate configurations.
    cat /configs/pg_hba.conf >> $PGDATA/pg_hba.conf
    cp /configs/postgresql.auto.conf.master $PGDATA/postgresql.auto.conf

    # start server
    pg_ctl -w start
}

setup_standby()
{
    until psql -h 172.22.0.10 -U $PGUSER -c '\q'; do
        >&2 echo "Postgres is unavailable - sleeping"
        sleep 1
    done

    # take backup
    pg_basebackup -h 172.22.0.10 -p 5432 -U $PGUSER -D $PGDATA -Fp -Xs -P

    # copy appropriate configurations.
    cp /configs/postgresql.auto.conf.standby $PGDATA/postgresql.auto.conf

    # start in standby mode
    touch $PGDATA/standby.signal

    # start server
    pg_ctl -w start
}

# create archive directory
mkdir -p $ARCHIVEDIR
mkdir -p ~/.ssh && chmod 0700 ~/.ssh

case $ROLE in
    master)
        setup_ssh_connection $PGUSER $STANDBY1_IP
        setup_master
        ;;
    standby)
        setup_ssh_connection $PGUSER $MASTER_IP
        setup_standby standby
        ;;
    *)
        echo "invalid option: $@"
        ;;
esac
