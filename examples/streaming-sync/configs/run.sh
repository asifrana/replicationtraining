#!/bin/bash

export REPLICATION="${REPLICATION:-ARCHIVING}"
export PGDATA="${PGDATA:-/var/lib/pgsql/12/data}"
export PGUSER="${PGUSER:-postgres}"
export PATH=/usr/pgsql-12/bin:$PATH
export ARCHIVEDIR="${ARCHIVEDIR:-$PGDATA/../archivedir}"

export MASTER_IP="${MASTER_IP:-172.22.0.10}"
export STANDBY1_IP="${STANDBY1_IP:-172.22.0.11}"
#export STANDBY2_IP="${STANDBY2_IP:-172.22.0.12}"

echo "=========================="
echo "REPLICATION: $REPLICATION"
echo "ROLE: $ROLE"
echo "PGUSER: $PGUSER"
echo "=========================="

#  USER, HOST
setup_ssh_connection()
{
    USER=$1
    HOST=$2
    ssh-keygen -t rsa -N '' -q -f ~/.ssh/id_rsa
    sshpass -p "$USER" ssh-copy-id -f $USER@$HOST

    status=$(ssh $USER@$HOST echo ok 2>&1)
    if [[ $status == ok ]] ; then
        echo "SSH connection ok"
    else
        echo "password-less ssh connection setup failed on $ROLE."
        exit
    fi
}


setup_master()
{
    # initialize cluster
    initdb

    # copy appropriate configurations.
    cat /configs/pg_hba.conf >> $PGDATA/pg_hba.conf
    cp /configs/postgresql.auto.conf.master $PGDATA/postgresql.auto.conf

    # start server
    pg_ctl -w start

    if [[ $REPLICATION == "STREAMINGSYNC" ]]; then
        # wait until standby's are active before turning master into synchronus mode
        until psql -h 172.22.0.11 -U $PGUSER -c '\q'; do
            >&2 echo "Postgres is unavailable - sleeping"
            sleep 1
        done

        # Turn synchronous_commit on
        sed -i "s/^#\(synchronous_commit.*\)/\1/g" $PGDATA/postgresql.auto.conf

        # add standby names to synchronous_standby_names list
        sed -i "s/^#\(synchronous_standby_names.*\)/\1/g" $PGDATA/postgresql.auto.conf

        # set replication_timeout, in case standby goes down or not accessible anymore
        #sed -i "s/^#replication_timeout\(.*\)/replication_timeout\1/g" $PGDATA/postgresql.auto.conf

        pg_ctl -w reload
    fi
}

setup_standby()
{
    until psql -h 172.22.0.10 -U $PGUSER -c '\q'; do
        >&2 echo "Postgres is unavailable - sleeping"
        sleep 1
    done

    # either take backup with -R or set primary_conninfo in the postgresql.conf

    # take backup
    pg_basebackup -h 172.22.0.10 -p 5432 -U $PGUSER -D $PGDATA -Fp -Xs -P

    # copy appropriate configurations.
    cp /configs/postgresql.auto.conf.standby $PGDATA/postgresql.auto.conf

    # start in standby mode
    touch $PGDATA/standby.signal

    sed -i "s/^\(primary_conninfo.*\)standby/\1$1/g" $PGDATA/postgresql.auto.conf

    # start server
    pg_ctl -w start
}

# create archive directory
mkdir -p $ARCHIVEDIR
mkdir -p ~/.ssh && chmod 0700 ~/.ssh

case $ROLE in
    master)
        setup_master
        ;;
    standby)
        setup_standby standby
        ;;
    standby2)
        setup_standby standby2
        ;;
    *)
        echo "invalid option: $@"
        ;;
esac
