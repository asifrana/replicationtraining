Replicatoin Training
====================

If you do not have a docker engine installed on your system, you may
visit their site for detailed installation instructions for your
platform. Here is the URL:
https://docs.docker.com/install/

Once you have installed and configured the engine as per the
documentation, you are good to start using it. So let's proceed on with
the launching dockers for replication training.

Clone this repository on your system using this command (or equivalent):
git clone http://gitlab.com/asifrana/replicationtraining.git

Change to cloned git repository on your system.
cd replicationtraining

cd docker
docker-compose up

This will build and start three containers. These containers are
assgined static IPs and have been named as follow:

172.22.0.10	master
172.22.0.11	standby
172.22.0.12	standby2

You can attach to the containers using a terminal session like:
docker exec -it master /bin/bash
docker exec -it standby /bin/bash
docker exec -it standby2 /bin/bash

Each of the containers have PostgreSQL version 12 installation already.
The binaries are located in:
/usr/pgsql-12

Following environment variables are set:
PGDATA=/var/lib/pgsql/12/data
PGPORT=5432

The default user is postgres. The password is also postgres.

Once done, do this to shutdown all three running containers:
docker-compose down


cd ..
cd examples

There are couple of subdirectories containing the docker setup. Each
contains a specific replication settings. i.e. streaming replication,
streaming replication with sync, with cascade. logical replication etc.
These are already configured, so you can do the experiments with them.

